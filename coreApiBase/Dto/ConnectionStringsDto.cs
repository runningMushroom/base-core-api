﻿namespace vueBuilderApi.Dto
{
    public class ConnectionStringsDto
    {
        public string DefaultConnection { get; set; }
    }
}