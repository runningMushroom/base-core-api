﻿namespace vueBuilderApi.Dto
{
    public class SystemSettingsDto
    {
        public string FrontendUrl { get; set; }
    }
}