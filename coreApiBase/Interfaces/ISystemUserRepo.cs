﻿using vueBuilderApi.Models;

namespace vueBuilderApi.Interfaces
{
    public interface ISystemUserRepo : IRepoBase<SystemUserModel>
    {
    }
}