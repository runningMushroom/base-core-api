﻿using vueBuilderApi.Models;

namespace vueBuilderApi.Interfaces
{
    public interface IUserRepo : IRepoBase<UserModel>
    {
    }
}